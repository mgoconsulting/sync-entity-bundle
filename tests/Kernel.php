<?php

namespace Test;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Test\Entity as E;

// phpcs:disable
require __DIR__.'/../vendor/autoload.php';
// phpcs:enable

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): iterable
    {
        return [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Symfony\Bundle\MonologBundle\MonologBundle(),
            new \Mgo\SyncEntityBundle\MgoSyncEntityBundle(),
        ];
    }

    protected function configureContainer(ContainerBuilder $builder, LoaderInterface $loader)
    {
        $dbPath = sys_get_temp_dir() . '/mgo-sync-entity-test.db';
        @unlink($dbPath);

        $builder->setParameter('sync_bundle_test_path', __DIR__);

        $builder->loadFromExtension('framework', [
            'secret' => 'TEST',
            'test' => true,
        ]);
        $builder->loadFromExtension('doctrine', [
            'dbal' => [
                'default_connection' => 'test',
                'connections' => [
                    'test' => [
                        'driver' => 'pdo_sqlite',
                        'path' => $dbPath,
                    ],
                ],
            ],
            'orm' => [
                'mappings' => [
                    'tests' => [
                        'type' => 'annotation',
                        'prefix' => 'Test\Entity',
                        'dir' => __DIR__ . '/Entity',
                    ],
                ],
            ],
        ]);
        $builder->loadFromExtension('mgo_sync_entity', [
            'mappings' => [
                'test_1' => [
                    'events' => [
                        'create' => [
                            'calls' => [
                                'source set call test' => [
                                    'condition' => 'source.getCallTest() == ""',
                                    'expression' => 'entity_manager.persist(source.setCallTest("CREATE")) or entity_manager.flush()',
                                ],
                            ],
                        ],
                        'update' => [
                            'calls' => [
                                'target set call test' => [
                                    'condition' => 'target.getCallTest() == ""',
                                    'expression' => 'entity_manager.persist(target.setCallTest("UPDATE")) or entity_manager.flush()',
                                ],
                            ],
                        ],
                        'delete' => [
                            'calls' => [
                                'test flush' => [
                                    'expression' => 'entity_manager.flush()',
                                ],
                            ],
                        ],
                    ],
                    'source' => [
                        'class' => E\SourceEntity::class,
                        'identifier' => 'id',
                    ],
                    'targets' => [
                        E\TargetEntity1::class => [
                            'source_id_reference' => 'sourceId',
                            'mapping' => [
                                'name' => 'source.getName()',
                                'onlyOnUpdate' => [
                                    'condition' => 'event == "postUpdate"',
                                    'expression' => 'source.getName() ~ " ONLY ON UPDATE"',
                                ],
                            ],
                            'source_mapping' => [
                                'targetId' => 'source.getId()',
                            ],
                        ],
                        E\TargetEntity2::class => [
                            'source_id_reference' => 'sourceId',
                            'mapping' => [
                                'name' => 'saved["targets"]["Test\\\Entity\\\TargetEntity1"].getName()',
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}
