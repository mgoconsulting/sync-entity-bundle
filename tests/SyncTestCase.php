<?php

namespace Test;

use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class SyncTestCase extends KernelTestCase
{
    /** @var \Doctrine\Persistence\ManagerRegistry */
    protected static $doctrine;

    public static function setUpBeforeClass(): void
    {
        // boot test kernel
        static::bootKernel(['debug' => self::isDebug()]);
        // get doctrine services
        self::$doctrine = self::$kernel->getContainer()->get('doctrine');
        // create db
        self::buildDb();
    }

    private static function buildDb(): void
    {
        $em = self::$doctrine->getManager();
        $connection = $em->getConnection();
        $database = $connection->getDatabase();

        $schemaManager = $connection->getSchemaManager();
        $schemaManager->dropDatabase($database);
        $schemaManager->createDatabase($database);

        $schemaTool = new SchemaTool($em);
        $schemaTool->createSchema($em->getMetadataFactory()->getAllMetadata());
    }

    protected static function isDebug(): bool
    {
        return \in_array('--debug', $_SERVER['argv'] ?? []);
    }
}
