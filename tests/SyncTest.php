<?php

namespace Test;

use Test\Entity\SourceEntity;
use Test\Entity\TargetEntity1;
use Test\Entity\TargetEntity2;

class SyncTest extends SyncTestCase
{
    public function testPostPersistEvent(): SourceEntity
    {
        $em = self::$doctrine->getManagerForClass(SourceEntity::class);
        $nbSource = count($em->getRepository(SourceEntity::class)->findAll());
        $nbTarget = count($em->getRepository(TargetEntity1::class)->findAll());

        $source = new SourceEntity();
        $source->setName($name = \uniqid());

        $em->persist($source);
        $em->flush();

        // source should be created successfully (+1 in the table)
        $this->assertCount(
            $nbSource + 1,
            $em->getRepository(SourceEntity::class)->findAll(),
            SourceEntity::class
        );
        // target should be sync and so created successfully (+1 in the table)
        $this->assertCount(
            $nbTarget + 1,
            $targets = $em->getRepository(TargetEntity1::class)->findAll(),
            TargetEntity1::class
        );
        // source targetId should be set
        $this->assertIsInt(
            $source->getTargetId(),
            'source targetId'
        );

        /** @var TargetEntity1 $target */
        $target = end($targets);
        $this->assertEquals($name, $target->getName(), 'Name');
        $this->assertEquals(null, $target->getOnlyOnUpdate(), 'onlyOnUpdate');

        // refresh values to make sure its been flushed
        $em->refresh($source);
        $em->refresh($target);

        // test calls
        $this->assertEquals('CREATE', $source->getCallTest(), 'Source call test');

        return $source;
    }

    public function testPostUpdateEvent(): SourceEntity
    {
        $em = self::$doctrine->getManagerForClass(SourceEntity::class);

        // create some data
        $this->testPostPersistEvent();
        // count data
        $nbSource = count($em->getRepository(SourceEntity::class)->findAll());
        $nbTarget = count($em->getRepository(TargetEntity1::class)->findAll());
        // get latest source and target sync'ed
        $source = $em->getRepository(SourceEntity::class)->findOneBy([], ['id' => 'desc']);
        $target = $em->getRepository(TargetEntity1::class)->findOneBy([], ['id' => 'desc']);
        $targetId = $source->getTargetId();

        // update source
        $source->setName($name = \uniqid());

        $em->persist($source);
        $em->flush();

        // source should be updated successfully (same number of rows in the table)
        $this->assertCount(
            $nbSource,
            $em->getRepository(SourceEntity::class)->findAll(),
            SourceEntity::class
        );
        // target should be updated successfully (same number of rows in the table)
        $this->assertCount(
            $nbTarget,
            $targets = $em->getRepository(TargetEntity1::class)->findAll(),
            TargetEntity1::class
        );
        // source targetId should be the same
        $this->assertEquals(
            $targetId,
            $source->getTargetId(),
            'source targetId'
        );

        /** @var TargetEntity1 $target */
        $target = end($targets);
        $this->assertEquals($name, $target->getName(), 'Name');
        $this->assertEquals("{$name} ONLY ON UPDATE", $target->getOnlyOnUpdate(), 'onlyOnUpdate');

        // test calls
        $this->assertEquals('UPDATE', $target->getCallTest(), 'Target call test');

        return $source;
    }

    public function testPostRemoveEvent(): void
    {
        $em = self::$doctrine->getManagerForClass(SourceEntity::class);

        // create some data
        $source = $this->testPostPersistEvent();
        $targetId = $source->getTargetId();

        // count data
        $nbSource = count($em->getRepository(SourceEntity::class)->findAll());
        $nbTarget = count($em->getRepository(TargetEntity1::class)->findAll());

        $em->remove($source);
        $em->flush();

        // source should be deleted successfully (-1 in the table)
        $this->assertCount(
            $nbSource - 1,
            $em->getRepository(SourceEntity::class)->findAll(),
            SourceEntity::class
        );
        // target should be deleted successfully (-1 in the table)
        $this->assertCount(
            $nbTarget - 1,
            $em->getRepository(TargetEntity1::class)->findAll(),
            TargetEntity1::class
        );
        $this->assertNull(
            $em->getRepository(TargetEntity1::class)->find($targetId),
            TargetEntity1::class . ' removed'
        );
    }

    public function testSavedValue(): void
    {
        $em = self::$doctrine->getManagerForClass(SourceEntity::class);

        // create some data
        $this->testPostPersistEvent();
        $targets1 = $em->getRepository(TargetEntity1::class)->findAll();
        $targets2 = $em->getRepository(TargetEntity2::class)->findAll();
        $this->assertEquals(
            end($targets1)->getName(),
            end($targets2)->getName()
        );
    }
}
