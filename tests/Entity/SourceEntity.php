<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="source")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class SourceEntity
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="target_id", type="integer", nullable=true)
     */
    private $targetId;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="call_test", type="string", nullable=false)
     */
    private $callTest = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTargetId(int $targetId): self
    {
        $this->targetId = $targetId;

        return $this;
    }

    public function getTargetId(): int
    {
        return $this->targetId;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setCallTest(string $value)
    {
        $this->callTest = $value;

        return $this;
    }

    public function getCallTest(): string
    {
        return $this->callTest;
    }
}
