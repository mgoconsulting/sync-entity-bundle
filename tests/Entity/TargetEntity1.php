<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="target_1")
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 */
class TargetEntity1
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="source_id", type="integer", nullable=false)
     */
    private $sourceId;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="only_on_update", type="string", nullable=true)
     */
    private $onlyOnUpdate = null;

    /**
     * @ORM\Column(name="call_test", type="string", nullable=false)
     */
    private $callTest = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setSourceId(int $sourceId): self
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    public function getSourceId(): int
    {
        return $this->sourceId;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setOnlyOnUpdate(?string $onlyOnUpdate): self
    {
        $this->onlyOnUpdate = $onlyOnUpdate;

        return $this;
    }

    public function getOnlyOnUpdate(): ?string
    {
        return $this->onlyOnUpdate;
    }

    public function setCallTest(string $value)
    {
        $this->callTest = $value;

        return $this;
    }

    public function getCallTest(): string
    {
        return $this->callTest;
    }
}
