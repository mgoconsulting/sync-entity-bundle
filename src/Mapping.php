<?php

namespace Mgo\SyncEntityBundle;

class Mapping
{
    /** @var string */
    private $syncName;

    /** @var string */
    private $className;

    /** @var array */
    private $sourceConfig;

    /** @var array */
    private $targetConfig;

    /** @var mixed */
    private $deletedSourceId;

    public function __construct(
        string $syncName,
        string $className,
        array $sourceConfig,
        array $targetConfig,
        $deletedSourceId = null
    ) {
        $this->syncName = $syncName;
        $this->className = $className;
        $this->sourceConfig = $sourceConfig;
        $this->targetConfig = $targetConfig;
        $this->deletedSourceId = $deletedSourceId;
    }

    public function getSyncName(): string
    {
        return $this->syncName;
    }

    public function getMapping(): array
    {
        return $this->targetConfig['mapping'] ?? [];
    }

    public function getTargetCondition(): ?string
    {
        return $this->targetConfig['condition'] ?? null;
    }

    public function getSourceMapping(): array
    {
        return $this->targetConfig['source_mapping'] ?? [];
    }

    public function getSourceIdReference(): ?string
    {
        return $this->targetConfig['source_id_reference'] ?? null;
    }

    public function getSourceIdentifier(): string
    {
        return $this->sourceConfig['identifier'];
    }

    public function getDeletedSourceId()
    {
        return $this->deletedSourceId;
    }
}
