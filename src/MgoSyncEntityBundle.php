<?php

namespace Mgo\SyncEntityBundle;

use Mgo\SyncEntityBundle\DependencyInjection\MgoSyncEntityExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

/**
 * Config Bundle.
 */
class MgoSyncEntityBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    protected function getContainerExtensionClass(): string
    {
        return MgoSyncEntityExtension::class;
    }
}
