<?php

namespace Mgo\SyncEntityBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class Configuration implements ConfigurationInterface
{
    public const EVENT_CREATE = 'create';
    public const EVENT_UPDATE = 'update';
    public const EVENT_DELETE = 'delete';
    public const EVENT_SOFT_DELETE = 'softdelete';

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mgo_sync_entity');
        // config component version
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode();
        } else {
            $root = $treeBuilder->root('mgo_sync_entity');
        }

        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $root */
        $root
            ->canBeDisabled()
            ->children()
                ->variableNode('presets')->end() // NOT USED! Just to set up YAML anchors
                ->arrayNode('exceptions')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('validation_exception')
                            ->defaultValue(ValidatorException::class)
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('mappings')
                    ->arrayPrototype() // sync names
                        ->canBeDisabled()
                        ->children()
                            ->arrayNode('events')
                                ->children()
                                    ->append($this->getEventNode(self::EVENT_CREATE))
                                    ->append($this->getEventNode(self::EVENT_UPDATE))
                                    ->append($this->getEventNode(self::EVENT_DELETE))
                                    ->append($this->getEventNode(self::EVENT_SOFT_DELETE))
                                ->end()
                            ->end()
                            ->arrayNode('source')
                                ->children()
                                    ->scalarNode('class')
                                        ->isRequired()
                                        ->cannotBeEmpty()
                                        ->validate()
                                            ->ifTrue(function ($class) {
                                                return !\class_exists($class);
                                            })
                                            ->thenInvalid('Class %s does not exists')
                                        ->end()
                                    ->end()
                                    ->scalarNode('identifier')
                                        ->defaultValue('id')
                                        ->cannotBeEmpty()
                                    ->end()
                                    ->scalarNode('condition')
                                        ->defaultFalse()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('targets')
                                ->validate()
                                    ->ifTrue(function ($targets) {
                                        foreach (array_keys($targets) as $class) {
                                            if (!\class_exists($class)) {
                                                return true;
                                            }
                                        }

                                        return false;
                                    })
                                    ->thenInvalid('Target class does not exists for: %s')
                                ->end()
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('identifier')
                                            ->defaultValue('id')
                                            ->cannotBeEmpty()
                                        ->end()
                                        ->scalarNode('source_id_reference')
                                            ->defaultNull()
                                        ->end()
                                        ->scalarNode('condition')
                                            ->defaultFalse()
                                        ->end()
                                        ->append($this->getExpressionsNode('mapping'))
                                        ->append($this->getExpressionsNode('source_mapping'))
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    private function getEventNode(string $name)
    {
        $treeBuilder = new TreeBuilder($name);

        if (method_exists($treeBuilder, 'getRootNode')) {
            $node = $treeBuilder->getRootNode();
        } else {
            // BC layer for symfony/config 4.1 and older
            $node = $treeBuilder->root($name);
        }

        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $node */
        $nodeBuilder = $node
            ->canBeEnabled()
            ->children()
                ->scalarNode('async_cached')
                    ->defaultNull()
                ->end()
                ->append($this->getExpressionsNode(
                    'calls',
                    function (NodeBuilder $builder) {
                        $builder->booleanNode('mute_sync')
                            ->defaultTrue()
                        ->end();
                    }
                ));

        if (self::EVENT_UPDATE === $name) {
            $nodeBuilder
                ->arrayNode('diff_disabled_fields')
                    ->scalarPrototype()->end()
                ->end();
        }

        if (self::EVENT_DELETE === $name || self::EVENT_SOFT_DELETE === $name) {
            $nodeBuilder
                ->booleanNode('flush')
                    ->defaultTrue()
                ->end();
        }
        $node->end();

        return $node;
    }

    private function getExpressionsNode(string $name, callable $appendChildren = null)
    {
        $treeBuilder = new TreeBuilder($name);

        if (method_exists($treeBuilder, 'getRootNode')) {
            $node = $treeBuilder->getRootNode();
        } else {
            // BC layer for symfony/config 4.1 and older
            $node = $treeBuilder->root($name);
        }
        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $node */
        $children = $node
            ->arrayPrototype()
                ->beforeNormalization()
                    ->ifTrue(function ($mapping) {
                        return \is_string($mapping);
                    })
                    ->then(function ($expression) {
                        return ['expression' => $expression];
                    })
                ->end()
                ->children()
                    ->scalarNode('expression')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->scalarNode('condition')
                        ->defaultFalse()
                        ->cannotBeEmpty()
                    ->end();

        if ($appendChildren && \is_callable($appendChildren)) {
            $appendChildren($children);
        }

        $children->end()->end();

        return $node;
    }
}
