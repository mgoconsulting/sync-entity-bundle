<?php

namespace Mgo\SyncEntityBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Mgo Sync Entity Extension.
 */
class MgoSyncEntityExtension extends Extension implements PrependExtensionInterface
{
    public const ALIAS = 'mgo_sync_entity';

    public function getAlias()
    {
        return self::ALIAS;
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        // validate config
        $config = $this->processConfiguration($configuration, $configs);
        // load yaml files
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        // set config in parameters
        $container->setParameter('mgo_sync_entity.configuration', $config);
    }

    public function prepend(ContainerBuilder $container)
    {
        $container->prependExtensionConfig(
            'monolog',
            [
                'channels' => ['sync'],
            ]
        );
    }
}
