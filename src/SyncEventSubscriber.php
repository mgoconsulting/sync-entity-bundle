<?php

namespace Mgo\SyncEntityBundle;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use Mgo\DoctrineExtension\Util\ChangeSetUtil;
use Mgo\SyncEntityBundle\Helper\EventHelper;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\PropertyAccess\PropertyAccess;

class SyncEventSubscriber implements EventSubscriber
{
    public const MAPPINGS_PER_SOURCES = '_mappings_per_sources';
    public const MAPPINGS_PER_TARGETS = '_mappings_per_targets';

    public static $amqp = false;

    /**
     * Number of iterations.
     *
     * @var array
     */
    public static $asyncCache = [];

    /**
     * @var array
     */
    public static $deletions = [];

    /**
     * Number of iterations.
     *
     * @var int
     */
    private static $iterations = 0;

    /**
     * Enable or disable the listener completely.
     *
     * @var bool
     */
    private static $enabled = true;

    /**
     * Map to check which source entity to back after targets are synced.
     *
     * @var array
     */
    public static $syncSourceBack = [];

    /**
     * Prevent infinite loops, when sources are syned back.
     *
     * @var array
     */
    private static $disabledSyncFor = [];

    /**
     * Calls to run.
     *
     * @var array
     */
    private static $calls = [];

    /** @var ContainerInterface */
    private $container;

    /** @var SyncService */
    private $syncService;

    /** @var LoggerInterface */
    private $logger;

    /** @var array */
    private $config;

    /** @var string */
    private $env;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->syncService = $container->get('mgo_sync_entity.service');
        $this->logger = $container->get('monolog.logger.sync');
        $this->config = $container->getParameter('mgo_sync_entity.configuration');
        $this->env = $container->getParameter('kernel.environment');
        $this->maxIteration = 1000;

        $this->config[self::MAPPINGS_PER_SOURCES] = $this->remapConfigPerSources();
        $this->config[self::MAPPINGS_PER_TARGETS] = $this->remapConfigPerTargets();

        self::$enabled = $this->config['enabled'];
    }

    public static function isEnabled(): bool
    {
        return self::$enabled;
    }

    public static function enabled(bool $value)
    {
        self::$enabled = $value;
    }

    public static function resetIterations(): void
    {
        self::$iterations = 0;
        self::$syncSourceBack = [];
        self::$disabledSyncFor = [];
        self::$deletions = [];
    }

    /** Note: Map sync events with doctrine */
    public function getSubscribedEvents()
    {
        return EventHelper::getSubscribedEvents();
    }

    public function __call($eventName, array $args)
    {
        if (self::$enabled && EventHelper::hasSubscribedEvent($eventName)) {
            if (Events::postFlush === $eventName) {
                return $this->runCalls();
            }

            $eventArgs = current($args);
            $object = $eventArgs->getObject();
            $className = get_class($object);

            try {
                // sync entity

                if (isset($this->config[self::MAPPINGS_PER_SOURCES][$className][$eventName])) {
                    ++self::$iterations;
                    if (self::$iterations > $this->maxIteration) {
                        throw new \Exception('Sync error: Too much sync iterations');
                    }
                    $syncId = $this->syncTarget($eventName, $eventArgs);
                    if (!$syncId) {
                        return;
                    }
                }
                // preRemove event is just here to save source id before delete
                // will delete on next postRemove event
                if (Events::preRemove === $eventName) {
                    return;
                }

                // sync source, after target has been synced
                // No need to sync source on remove.

                if (
                    Events::postRemove !== $eventName &&
                    SoftDeleteableListener::POST_SOFT_DELETE !== $eventName &&
                    isset($this->config[self::MAPPINGS_PER_TARGETS][$className])
                ) {
                    $this->syncSourceBack($eventName, $eventArgs);
                }

                // enable calls for this sync
                if (isset($syncId) && $syncId && isset(self::$calls[$syncId])) {
                    foreach (self::$calls[$syncId] as &$call) {
                        $call['_run'] = true;
                    }
                }
            } catch (\Exception $e) {
                // validation exceptions always throw
                $validationExceptionClass = $this->config['exceptions']['validation_exception'] ?? false;
                if (
                    ($validationExceptionClass && $e instanceof $validationExceptionClass)
                    || 'prod' !== $this->env
                ) {
                    throw $e;
                }
                $this->logger->critical(
                    'SYNC:' . $e->getMessage(),
                    [
                        'eventName' => $eventName,
                        'className' => $className,
                        'id' => \is_callable([$object, 'getId']) ? $object->getId() : null,
                    ]
                );
            }
        }
    }

    private function syncTarget(string $eventName, LifecycleEventArgs $args): ?string
    {
        $source = $args->getObject();
        $sourceClass = get_class($source);
        $syncId = \uniqid(\sprintf('%s_%s_', $eventName, \str_replace('\\', '', $sourceClass)));

        // check if target is mapped in sync process definition
        if (isset($this->config[self::MAPPINGS_PER_SOURCES][$sourceClass][$eventName])) {
            $em = $args->getEntityManager();
            $pa = $this->getPropertyAccessor();
            $syncNames = $this->config[self::MAPPINGS_PER_SOURCES][$sourceClass][$eventName];
            foreach ($syncNames as $syncName) {
                $conf = $this->config['mappings'][$syncName];

                // check for update changes diff
                $changeSet = [];
                if (in_array($eventName, [Events::preUpdate, Events::postUpdate])) {
                    $changeSetUtil = new ChangeSetUtil(
                        $em,
                        $source,
                        [
                            'strict_type_cast_mapping' => [
                                'boolean' => 'boolean',
                                'string' => 'string',
                                'int' => 'int',
                                'bigint' => 'string',
                                'float' => 'string',
                            ],
                            'disabled' => $conf['events']['update']['diff_disabled_fields'],
                        ]
                    );
                    $changeSet = $changeSetUtil->getChangeSet(true);
                    if (!SyncEventSubscriber::$amqp && !$changeSet) {
                        continue;
                    }
                }

                $sourceId = $pa->getValue($source, $conf['source']['identifier']);
                $sourceHash = spl_object_hash($source);

                // need to save source id before real delete in event postDelete
                if (Events::preRemove === $eventName) {
                    self::$deletions[$sourceClass][$sourceHash] = $sourceId;

                    return null;
                }

                // condition to sync
                if (
                    Events::postRemove !== $eventName &&
                    SoftDeleteableListener::POST_SOFT_DELETE !== $eventName &&
                    !$this->syncService->canSync($syncName, $source)
                ) {
                    continue;
                }

                if ($sourceId && isset(self::$disabledSyncFor[$sourceClass][$sourceId])) {
                    // do nothing and disabled for this entity
                    unset(self::$disabledSyncFor[$sourceClass][$sourceId]);
                    continue;
                }

                $event = EventHelper::getEventByName($eventName);
                $async = $conf['events'][$event]['async_cached'] ?? false;
                if (
                    !SyncEventSubscriber::$amqp
                    && $async
                    && $this->getExpressionLanguage()->evaluate($async, ['container' => $this->container])
                ) {
                    self::$iterations = 0;
                    self::$asyncCache[] = [
                        'eventName' => $eventName,
                        'className' => $sourceClass,
                        'id' => ((Events::postRemove === $eventName || SoftDeleteableListener::POST_SOFT_DELETE === $eventName) ? (self::$deletions[$sourceClass][$sourceHash] ?? null) : $sourceId),
                        'changeSet' => $changeSet,
                    ];

                    return null;
                }

                // for each targets
                foreach ($conf['targets'] as $targetClass => $targetConf) {
                    $mapping = new Mapping(
                        $syncName,
                        $targetClass,
                        $conf['source'],
                        $targetConf,
                        (self::$deletions[$sourceClass][$sourceHash] ?? null) // source id for deletion
                    );

                    // cherche la target partir de l'event (insert, update ou delete)
                    $target = $this->syncService->fetchTarget($eventName, $source, $targetClass, $mapping);
                    if ($target) {
                        // will sync the source back (with the 'source_mapping' config)
                        // BEFORE syncTarget call important !
                        self::$syncSourceBack[$targetClass][$sourceId] = [
                            'target' => $target,
                            'source' => $source,
                            'sync_name' => $syncName,
                        ];

                        $changeSetUtil = new ChangeSetUtil($em, $source);
                        $this->syncService->syncTarget($eventName, $mapping, $source, $target);
                        self::$calls[$syncId][$targetClass] = [
                            '_run' => false,
                            'eventName' => $eventName,
                            'mapping' => $mapping,
                            'source' => $source,
                            'target' => $target,
                            // send targetId and sourceId to avoid doing the identifier config thing
                            'sourceId' => ($source ? $sourceId : null),
                            'targetId' => ($target ? $pa->getValue($target, $targetConf['identifier']) : null),
                            'changeSetUtil' => $changeSetUtil,
                        ];
                    }
                }

                if (Events::postRemove === $eventName || SoftDeleteableListener::POST_SOFT_DELETE === $eventName) {
                    unset(self::$deletions[$sourceClass][$syncName]);
                }
            }
        }

        return $syncId;
    }

    private function syncSourceBack(string $eventName, LifecycleEventArgs $args)
    {
        $target = $args->getObject();
        $targetClass = get_class($target);

        if (isset(self::$syncSourceBack[$targetClass])) {
            $syncSourceBack = self::$syncSourceBack[$targetClass];
            foreach ($syncSourceBack as $sourceId => $values) {
                if (isset($values['target']) && $values['target'] === $target) {
                    $conf = $this->config['mappings'][$values['sync_name']];
                    $mapping = new Mapping(
                        $values['sync_name'],
                        $targetClass,
                        $conf['source'],
                        $conf['targets'][$targetClass] ?? []
                    );
                    self::$disabledSyncFor[\get_class($values['source'])][$sourceId] = true;
                    $this->syncService->syncSourceBack($eventName, $mapping, $values['source'], $target);
                    unset(self::$syncSourceBack[$targetClass][$sourceId]);
                }
            }
        }
    }

    private function runCalls()
    {
        $calls = self::$calls;
        foreach ($calls as $syncId => $syncCalls) {
            foreach ($syncCalls as $targetClass => $call) {
                if (!$call['_run']) {
                    continue;
                }

                // get source values from call
                if ($source = $call['source']) {
                    $sourceClass = \get_class($source);
                    $sourceId = $call['sourceId'];
                }
                // get target values from call
                if ($target = $call['target']) {
                    $targetClass = \get_class($target);
                    $targetId = $call['sourceId'];
                }

                // disable sync for both source and target
                if (isset($sourceClass)) {
                    self::$disabledSyncFor[$sourceClass][$sourceId] = true;
                }
                if (isset($targetClass)) {
                    self::$disabledSyncFor[$targetClass][$targetId] = true;
                }

                $this->syncService->calls(
                    $call['eventName'],
                    $call['mapping'],
                    $call['changeSetUtil'],
                    $source,
                    $target
                );

                // re-enable sync for both source and target

                if (isset($sourceClass)) {
                    unset(self::$disabledSyncFor[$sourceClass][$sourceId]);
                }
                if (isset($targetClass)) {
                    unset(self::$disabledSyncFor[$targetClass][$targetId]);
                }
                unset(self::$calls[$syncId][$targetClass]);
            }
        }
    }

    private function remapConfigPerSources(): array
    {
        $mapped = [];
        foreach ($this->config['mappings'] as $syncName => $conf) {
            if ($conf['enabled']) {
                foreach ($conf['events'] as $event => $eventConf) {
                    if ($eventConf['enabled'] ?? false) {
                        $event = EventHelper::getSubscribedEventByName($event);
                        $mapped[$conf['source']['class']][$event][] = $syncName;
                        if (Events::postRemove === $event || SoftDeleteableListener::POST_SOFT_DELETE === $event) {
                            $mapped[$conf['source']['class']][Events::preRemove][] = $syncName;
                        }
                    }
                }
            }
        }

        return $mapped;
    }

    private function remapConfigPerTargets(): array
    {
        $mapped = [];
        foreach ($this->config['mappings'] as $syncName => $conf) {
            if ($conf['enabled']) {
                foreach ($conf['targets'] as $targetClass => $targetConf) {
                    if (isset($targetConf['source_mapping']) && $targetConf['source_mapping']) {
                        $mapped[$targetClass][] = $syncName;
                    }
                }
            }
        }

        return $mapped;
    }

    private function getPropertyAccessor()
    {
        // creates property accessor
        $builder = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex();
        if (\method_exists($builder, 'enableExceptionOnInvalidPropertyPath')) {
            $builder->enableExceptionOnInvalidPropertyPath();
        }

        return $builder->getPropertyAccessor();
    }

    private function getExpressionLanguage(): ExpressionLanguage
    {
        return new ExpressionLanguage();
    }
}
