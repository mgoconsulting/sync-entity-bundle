<?php

namespace Mgo\SyncEntityBundle\Helper;

use Doctrine\ORM\Events;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use Mgo\SyncEntityBundle\DependencyInjection\Configuration;

class EventHelper
{
    private static $eventsMap = [
        Configuration::EVENT_CREATE => Events::postPersist,
        Configuration::EVENT_UPDATE => Events::postUpdate,
        Configuration::EVENT_DELETE => Events::postRemove,
        Configuration::EVENT_SOFT_DELETE => SoftDeleteableListener::POST_SOFT_DELETE,
    ];

    public static function getSubscribedEvents(): array
    {
        return \array_merge(
            \array_values(self::$eventsMap),
            [
                Events::preRemove, // to catch target id from deleted source before remove
                Events::postFlush, // for calls
            ]
        );
    }

    public static function hasSubscribedEvent(string $subscribedEvent): bool
    {
        // call function getSubscribedEvents() important to include preRemove
        return \in_array($subscribedEvent, self::getSubscribedEvents());
    }

    public static function getSubscribedEventByName(string $event)
    {
        return self::$eventsMap[$event] ?? false;
    }

    public static function getEventByName(string $subscribedEvent)
    {
        // call property $eventsMap important to NOT include preRemove
        return \array_search($subscribedEvent, self::$eventsMap);
    }
}
