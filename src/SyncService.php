<?php

namespace Mgo\SyncEntityBundle;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Events;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use Mgo\DoctrineExtension\Util\ChangeSetUtil;
use Mgo\SyncEntityBundle\Helper\EventHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\PropertyAccess as PA;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class SyncService
{
    /** @var array */
    private static $saved = [];

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function clearSaved()
    {
        self::$saved = [];
    }

    public function varExport($var)
    {
        if (\is_object($var)) {
            /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
            $doctrine = $this->container->get('doctrine');
            $id = $identifier = null;
            if ($em = $doctrine->getManagerForClass($className = \get_class($var))) {
                $meta = $em->getClassMetadata($className);
                if ($meta->identifier) {
                    $pa = $this->getPropertyAccessor();
                    $identifier = current($meta->identifier);
                    if ($pa->isReadable($var, $identifier)) {
                        $id = $pa->getValue($var, $identifier);
                    }
                }
            }

            return \sprintf('Object%s %s', ($id ? " {$identifier}#{$id}" : ''), $className);
        }

        return var_export($var, true);
    }

    public function canSync(string $syncName, object $source): bool
    {
        $canSync = true;
        // condition to sync
        $condition = $this->getSyncConfig("[mappings][{$syncName}][source][condition]");

        if ($condition) {
            $elValues = [
                'source' => $source,
                'entity_manager' => $this->container->get('doctrine')->getManagerForClass(get_class($source)),
                'container' => $this->container,
                'environment' => $this->container->getParameter('kernel.environment'),
            ];

            $canSync = (bool) $this->getExpressionLanguage()->evaluate($condition, $elValues);
        }

        return $canSync;
    }

    public function fetchTarget(string $eventName, object $source, string $targetClass, Mapping $mapping): ?object
    {
        if (Events::postPersist === $eventName) {
            return new $targetClass();
        }

        /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
        $doctrine = $this->container->get('doctrine');

        // on va cherche l'id de la source dans les deletions
        if (Events::postRemove === $eventName || SoftDeleteableListener::POST_SOFT_DELETE === $eventName) {
            if (is_null($sourceId = $mapping->getDeletedSourceId())) {
                return null;
            }
        } else {
            // creates property accessor
            $pa = $this->getPropertyAccessor();
            // get source id from identifier
            $sourceId = $pa->getValue($source, $mapping->getSourceIdentifier());
        }

        // fetch target from source id
        $condition = $mapping->getTargetCondition();

        if ($condition) {
            foreach (
                $doctrine->getRepository($targetClass)->findBy([
                    $mapping->getSourceIdReference() => $sourceId,
                ]) as $target
            ) {
                $elValues = [
                    'source' => $source,
                    'target' => $target,
                    'entity_manager' => $this->container->get('doctrine')->getManagerForClass(get_class($source)),
                    'container' => $this->container,
                    'environment' => $this->container->getParameter('kernel.environment'),
                ];

                if ((bool) $this->getExpressionLanguage()->evaluate($condition, $elValues)) {
                    return $target;
                }
            }
        } else {
            return $doctrine->getRepository($targetClass)->findOneBy([
                $mapping->getSourceIdReference() => $sourceId,
            ]);
        }

        return null;
    }

    public function syncTarget(string $eventName, Mapping $mapping, object $source, object &$target): void
    {
        $syncName = $mapping->getSyncName();
        /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        // entity managers
        $targetClass = get_class($target);
        $em = $doctrine->getManagerForClass($targetClass);

        if (Events::postRemove === $eventName || SoftDeleteableListener::POST_SOFT_DELETE === $eventName) {
            if ($em && $this->getSyncConfig("[mappings][{$syncName}][events][delete][flush]")) {
                $em->remove($target);
                $em->flush();
            }

            return;
        }

        // creates property accessor
        $pa = $this->getPropertyAccessor();
        // creates expression langugae
        $el = $this->getExpressionLanguage();
        // default values to pass to the ExpressionLanguage
        $elValues = [
            'event' => $eventName,
            'source' => $source,
            'target' => $target,
            'entity_manager' => $em ?: $doctrine->getManager(),
            'container' => $this->container,
            'now' => new \DateTime(),
            'environment' => $this->container->getParameter('kernel.environment'),
            'saved' => (self::$saved[spl_object_hash($source)] ?? []),
        ];

        foreach ($mapping->getMapping() as $path => $map) {
            // condition to set the value
            if ($map['condition']) {
                $condition = (bool) $el->evaluate($map['condition'], $elValues);
                if (!$condition) {
                    continue;
                }
            }

            $newValue = $el->evaluate($map['expression'], $elValues);
            $pa->setValue($target, $path, $newValue);
        }

        // set source id in target (if enabled)
        if ($sourceIdReference = $mapping->getSourceIdReference()) {
            $id = $pa->getValue($source, $mapping->getSourceIdentifier());
            $pa->setValue($target, $sourceIdReference, $id);
        }

        $this->validate($target);
        if ($em) {
            $em->persist($target);
            $em->flush();
        }

        self::$saved[spl_object_hash($source)]['targets'][ClassUtils::getRealClass($targetClass)] = $target;
    }

    public function syncSourceBack(string $eventName, Mapping $mapping, object $source, object &$target): void
    {
        if (Events::postRemove === $eventName || SoftDeleteableListener::POST_SOFT_DELETE === $eventName) {
            return;
        }

        /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        // entity managers
        $em = $doctrine->getManagerForClass(get_class($source));
        // creates property accessor
        $pa = $this->getPropertyAccessor();
        // creates expression langugae
        $el = $this->getExpressionLanguage();
        // default values to pass to the ExpressionLanguage
        $elValues = [
            'event' => $eventName,
            'source' => $source,
            'target' => $target,
            'entity_manager' => $em,
            'container' => $this->container,
            'now' => new \DateTime(),
            'environment' => $this->container->getParameter('kernel.environment'),
        ];

        foreach ($mapping->getSourceMapping() as $path => $map) {
            // condition to set the value
            if ($map['condition']) {
                $condition = (bool) $el->evaluate($map['condition'], $elValues);
                if (!$condition) {
                    continue;
                }
            }

            $newValue = $el->evaluate($map['expression'], $elValues);
            $pa->setValue($source, $path, $newValue);
        }

        $em->persist($source);
        $em->flush();
    }

    public function calls(
        string $eventName,
        Mapping $mapping,
        ChangeSetUtil $changeSetUtil,
        object $source = null,
        object $target = null
    ): void {
        $syncName = $mapping->getSyncName();
        /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        // creates expression langugae
        $el = $this->getExpressionLanguage();
        // default values to pass to the ExpressionLanguage
        $elValues = [
            'source' => $source,
            'source_class' => ($source ? \get_class($source) : null),
            'target' => $target,
            'target_class' => ($target ? \get_class($target) : null),
            'entity_manager' => $doctrine->getManager(),
            'container' => $this->container,
            'now' => new \DateTime(),
            'environment' => $this->container->getParameter('kernel.environment'),
            'saved' => (self::$saved[spl_object_hash($source)] ?? []),
            'changeSetUtil' => $changeSetUtil,
        ];

        $calls = $this->getSyncConfig(\sprintf(
            '[mappings][%s][events][%s][calls]',
            $syncName,
            EventHelper::getEventByName($eventName)
        ));
        foreach ($calls as $name => $call) {
            // condition to set the value
            if ($call['condition']) {
                $condition = (bool) $el->evaluate($call['condition'], $elValues);
                if (!$condition) {
                    continue;
                }
            }

            if ($call['mute_sync']) {
                SyncEventSubscriber::enabled(false);
            }
            $el->evaluate($call['expression'], $elValues);
            SyncEventSubscriber::enabled(true);
        }
    }

    private function getSyncConfig($path = false)
    {
        $config = $this->container->getParameter('mgo_sync_entity.configuration');

        return $path ? $this->getPropertyAccessor()->getValue($config, $path) : $config;
    }

    private function getPropertyAccessor(): PA\PropertyAccessor
    {
        // creates property accessor
        $builder = PA\PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex();
        if (\method_exists($builder, 'enableExceptionOnInvalidPropertyPath')) {
            $builder->enableExceptionOnInvalidPropertyPath();
        }

        return $builder->getPropertyAccessor();
    }

    private function getExpressionLanguage(): ExpressionLanguage
    {
        return new ExpressionLanguage();
    }

    private function validate(object $entity)
    {
        /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $errors */
        $errors = $this->container->get('validator')->validate($entity);
        if (count($errors) > 0) {
            $exceptionClass = $this->getSyncConfig('[exceptions][validation_exception]');
            // \ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException
            throw new $exceptionClass($errors);
        }
    }
}
