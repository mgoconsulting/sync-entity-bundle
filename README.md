# **MGO Sync Entity Bundle**


## **Installation**

#### Installation with composer
```sh
composer config repositories.mgoconsulting/sync-entity-bundle git https://bitbucket.org/mgoconsulting/sync-entity-bundle.git
composer require mgoconsulting/sync-entity-bundle
```
#### Enable the bundle

In *config/bundles.php*
```php
<?php
return [
    ...
    Mgo\SyncEntityBundle\MgoSyncEntityBundle::class => ['prod' => true],
];
```
#### Enable the extension

In *config/packages/mgo_sync_entity.yaml*
```yaml
mgo_sync_entity:
    mappings:
        App\FromClass:
            App\TargetClass1:
                map:
                    property: property
                    property.path: other.path
            App\TargetClass2:
                map:
                    property.path: other.path
```  
